////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////
//////    usb_mib.h
//////
//////        USB Driver for Master PIC on MIB Board
//////
//////                    Mohammad Jamal Mohiuddin
//////                    NCR, Inc
//////                    07/19/2018
//////
//////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */



#ifndef _USB_MIB_H
#define _USB_MIB_H

#define USB_INTERFACE_CLASS_VENDOR_SPECIFIC	0xff
#define USB_INTERFACE_SUBCLASS_UNDEFINED	0x00

#define USB_INTERFACE_CLASS_APPLICATION_SPECIFIC_INTERFACE	0xFE
#define USB_INTERFACE_SUBCLASS_DFU	0x01

#define INTERFACE_DOWNLOAD	0
#define INTERFACE_GENERAL	1
#define INTERFACE_TOUCH		2
#define INTERFACE_MSR		3

#define EP_OUT_GEN_CMD		0x01
#define EP_IN_GEN_CMD		0x81

#define DFU_DETACH    0


#ifdef DEBUG

//struct usb_device_descriptor
#define DUMP_USB_DEVICE_DESCRIPTOR( d ) \
{\
	pr_info("USB_DEVICE_DESCRIPTOR:\n"); \
	pr_info("----------------------\n"); \
	pr_info("bLength: 0x%x\n", d.bLength); \
	pr_info("bDescriptorType: 0x%x\n", d.bDescriptorType); \
	pr_info("bcdUSB: 0x%x\n", d.bcdUSB); \
	pr_info("bDeviceClass: 0x%x\n", d.bDeviceClass); \
	pr_info("bDeviceSubClass: 0x%x\n", d.bDeviceSubClass); \
	pr_info("bDeviceProtocol: 0x%x\n", d.bDeviceProtocol); \
	pr_info("bMaxPacketSize0: 0x%x\n", d.bMaxPacketSize0); \
	pr_info("idVendor: 0x%x\n", d.idVendor); \
	pr_info("idProduct: 0x%x\n", d.idProduct); \
	pr_info("bcdDevice: 0x%x\n", d.bcdDevice); \
	pr_info("iManufacturer: 0x%x\n", d.iManufacturer); \
	pr_info("iProduct: 0x%x\n", d.iProduct); \
	pr_info("iSerialNumber: 0x%x\n", d.iSerialNumber); \
	pr_info("bNumConfigurations: 0x%x\n", d.bNumConfigurations); \
	pr_info("\n"); \
}


//struct usb_config_descriptor
#define DUMP_USB_CONFIGURATION_DESCRIPTOR( c ) \
{\
	pr_info("USB_CONFIGURATION_DESCRIPTOR:\n"); \
	pr_info("-----------------------------\n"); \
	pr_info("bLength: 0x%x\n", c.bLength); \
	pr_info("bDescriptorType: 0x%x\n", c.bDescriptorType); \
	pr_info("wTotalLength: 0x%x\n", c.wTotalLength); \
	pr_info("bNumInterfaces: 0x%x\n", c.bNumInterfaces); \
	pr_info("bConfigurationValue: 0x%x\n", c.bConfigurationValue); \
	pr_info("iConfiguration: 0x%x\n", c.iConfiguration); \
	pr_info("bmAttributes: 0x%x\n", c.bmAttributes); \
	pr_info("bMaxPower: 0x%x\n", c.bMaxPower); \
	pr_info("\n"); \
}

//struct usb_interface_descriptor
#define DUMP_USB_INTERFACE_DESCRIPTOR( i ) \
{\
	pr_info("USB_INTERFACE_DESCRIPTOR:\n"); \
	pr_info("-----------------------------\n"); \
	pr_info("bLength: 0x%x\n", i.bLength); \
	pr_info("bDescriptorType: 0x%x\n", i.bDescriptorType); \
	pr_info("bInterfaceNumber: 0x%x\n", i.bInterfaceNumber); \
	pr_info("bAlternateSetting: 0x%x\n", i.bAlternateSetting); \
	pr_info("bNumEndpoints: 0x%x\n", i.bNumEndpoints); \
	pr_info("bInterfaceClass: 0x%x\n", i.bInterfaceClass); \
	pr_info("bInterfaceSubClass: 0x%x\n", i.bInterfaceSubClass); \
	pr_info("bInterfaceProtocol: 0x%x\n", i.bInterfaceProtocol); \
	pr_info("iInterface: 0x%x\n", i.iInterface); \
	pr_info("\n"); \
}

//struct usb_endpoint_descriptor
#define DUMP_USB_ENDPOINT_DESCRIPTOR( e ) \
{\
	pr_info("USB_ENDPOINT_DESCRIPTOR:\n"); \
	pr_info("------------------------\n"); \
	pr_info("bLength: 0x%x\n", e.bLength); \
	pr_info("bDescriptorType: 0x%x\n", e.bDescriptorType); \
	pr_info("bEndPointAddress: 0x%x\n", e.bEndpointAddress); \
	pr_info("bmAttributes: 0x%x\n", e.bmAttributes); \
	pr_info("wMaxPacketSize: 0x%x\n", e.wMaxPacketSize); \
	pr_info("bInterval: 0x%x\n", e.bInterval); \
	pr_info("\n"); \
}

#else
#define DUMP_USB_DEVICE_DESCRIPTOR( p )
#define DUMP_USB_CONFIGURATION_DESCRIPTOR( c )
#define DUMP_USB_INTERFACE_DESCRIPTOR( i, _index )
#define DUMP_USB_ENDPOINT_DESCRIPTOR( e )

#endif  //DEBUG

struct email_port_data
{
	struct mutex data_lock;	
	unsigned char read_data[1024];
	int data_index_low;
	int data_index_high;
}__attribute__((packed));

#endif //_USB_MIB_H_
