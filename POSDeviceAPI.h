///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////
////    POSDeviceAPI.h
////
////        Defines exported resources of the "POS" Device included in
////        the RadPC_Controller.dll driver.
////
////                Greg Tinney
////                Radiant Systems, Inc.
////                3/15/06
////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


#if !defined( _POS_DEVICE_API_H )
#define _POS_DEVICE_API_H
#include "pantherusbioctl.h"


#define RADS_GET_APPSIGNATURE                               0       // get the signature placed on the code in the app
#define RADS_GET_APPREVISION                                1       // get the revision placed on the code in the app
#define RADS_GET_APPWORD                                    2       // get a word from the app's program memory (backdoor)
#define RADS_GET_BOOTLOADREV                                3       // get the revision of the bootloader
#define RADS_GET_VALIDITY                                   4       // find out whether the FW is valid
#define RADS_JUMP_APPLICATION	                            5       // jump to the application stored in the application sectors
#define RADS_SWITCH_TO_DFU                                  6       // switch to the DFU from the application

#define RADS_GET_STATE_BB                                   7       // get state of the bumblebee
#define RADS_SET_STATE_BB                                   8       // set the state of the bumblebee
#define RADS_READ_EEPROM	                                9       // read location from eeprom
#define RADS_WRITE_EEPROM                                   10      // write location on eeprom
#define RADS_GET_PROXIMITY		                            11      // proximity detector value
#define RADS_GET_PLATFORM_TYPE                              12      // get platform type

struct piclet_485_mode_opts
{
    unsigned char  piclet_address;
    unsigned int  baud_rate;
}__attribute__((packed));

//
// PICLET_CURRENT_LEVEL *
//
//   Formula for DAC - Vout = 5x/32
//
#define PICLET_CURRENT_LEVEL_0_MA  0
#define PICLET_CURRENT_LEVEL_5_MA  3
#define PICLET_CURRENT_LEVEL_10_MA 6
#define PICLET_CURRENT_LEVEL_15_MA 9
#define PICLET_CURRENT_LEVEL_20_MA 13
#define PICLET_CURRENT_LEVEL_25_MA 16
#define PICLET_CURRENT_LEVEL_30_MA 19
#define PICLET_CURRENT_LEVEL_35_MA 22
#define PICLET_CURRENT_LEVEL_40_MA 25
#define PICLET_CURRENT_LEVEL_45_MA 29 //[JCT 05/08/2014] WIT 693010 - Correcting 45 mA drive level
#define PICLET_CURRENT_LEVEL_50_MA 31

struct piclet_curloop_mode_opts
{
    unsigned char  piclet_address;
    unsigned char  current_loop_mode;
    unsigned char  current_level;
}__attribute__((packed));

// [JCT 05/30/2014] WIT 691889 - Implement EMAIL port functionality
struct piclet_email_port_data
{
    unsigned int  num_bytes;
    unsigned char data[512];
}__attribute__((packed));


// [JCT 04/23/2014] WIT 689935 - Macroizing MIB location constants
#define MIB_LOCATION_FIRST   1
#define MIB_LOCATION_SECOND  0

#define GEN_MAGIC_NUMBER	0x21

#define GEN_IOCTL_GET_APPSECTOR_REVISION	_IOR(GEN_MAGIC_NUMBER, 1, \
                                                 unsigned short)
#define GEN_IOCTL_GET_MIB_BOARD_LOCATION	_IOR(GEN_MAGIC_NUMBER, 2, \
                                                 unsigned char)
#define GEN_IOCTL_SET_PICLET_485_PORT_MODE	_IOW(GEN_MAGIC_NUMBER, 3, \
                                                 struct piclet_485_mode_opts)
#define GEN_IOCTL_SET_PICLET_CURLOOP_PORT_MODE	_IOW(GEN_MAGIC_NUMBER, 4, \
                                               struct piclet_curloop_mode_opts)
#define GEN_IOCTL_EMAIL_PORT_WRITE		_IOW(GEN_MAGIC_NUMBER, 5, unsigned char)
#define GEN_IOCTL_EMAIL_PORT_QUEUE_INIT		_IO(GEN_MAGIC_NUMBER, 6)
#define GEN_IOCTL_EMAIL_PORT_READ		_IOR(GEN_MAGIC_NUMBER, 7, \
                                             struct piclet_email_port_data)
#endif
