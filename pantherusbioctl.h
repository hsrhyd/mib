#if !defined(__PANTHER_USB_IOCTL__)
#define __PANTHER_USB_IOCTL__

#define PANTHER_MAGIC_NUMBER    0x50 //'P'

struct usb_info {
    unsigned int parent_dev_num;
    unsigned int bus_number;
}__attribute__((packed));


#define PANTH_IOCTL_GET_USB_DEV_INFO     _IOR(PANTHER_MAGIC_NUMBER, 1, \
                                               struct usb_info)

#endif
