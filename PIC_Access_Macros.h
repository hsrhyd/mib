///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////
////    PIC_Access_Macros.h
////
////        Put any common PIC macros here that are not auto-generated
////        by Hex Array application
////
////            [GST 9/11/13] - WIT 683998.
////
////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


// [GST 9/13/13] - WIT 683998. Moving some stuff from generated *.h file so that we can follow linked app.c inclusion model
//  used in Windows driver build.
#define PIC_APPSECTOR_START_ADDRESS   0x3000
#define PIC_APPSECTOR_SIZE            0xcc00


#define PIC_APPSECTOR_REVISION_ROM_ADDRESS      0x00302C    // 2-bytes
#define MAX_DOWNLOAD_BLOCK_SIZE                 64


//-
// [GST 6/6/05] - HACK HACK - In future, need to add these definitions to the auto-generated SilverFW.h and PIC_FW.h files!!!
//
#ifndef APP_REVISION_STMICRO
    #define APP_REVISION_STMICRO  (ucAutoFirmware[0xEFE2-FIRMWARE_START_ADDRESS] << 8 | ucAutoFirmware[0xEFE3-FIRMWARE_START_ADDRESS])
#endif // #ifndef APP_REVISION_STMICRO

#ifndef APP_REVISION_PIC
    #define APP_REVISION_PIC      (ucPICAppsectorFirmware[PIC_APPSECTOR_REVISION_ROM_ADDRESS+1-PIC_APPSECTOR_START_ADDRESS] << 8 | ucPICAppsectorFirmware[PIC_APPSECTOR_REVISION_ROM_ADDRESS-PIC_APPSECTOR_START_ADDRESS])
#endif // #ifndef APP_REVISION_PIC


#ifndef APP_REVISION_PIC18F66J50
    #define APP_REVISION_PIC18F66J50      (ucPICAppsectorFirmware[PIC_APPSECTOR_REVISION_ROM_ADDRESS+1-PIC_APPSECTOR_START_ADDRESS] << 8 | ucPICAppsectorFirmware[PIC_APPSECTOR_REVISION_ROM_ADDRESS-PIC_APPSECTOR_START_ADDRESS])
#endif // #ifndef APP_REVISION_PIC18F66J50


#ifndef DOWNLOAD_BLOCK_SIZE_PIC18F66J50
    #define DOWNLOAD_BLOCK_SIZE_PIC18F66J50     64
#endif // #ifndef DOWNLOAD_BLOCK_SIZE_PIC

#ifndef PIC18F66J50_APPSECTOR_SIZE
    //                                                  (64 Total flash size) - (12 Bootsector size) - (1 protected area)
    #define PIC18F66J50_APPSECTOR_SIZE          (1024 * (64 - 12 - 1))
#endif // #ifndef DOWNLOAD_BLOCK_SIZE_PIC


#define RADPC_DOWNLOAD_CHECKSUM_SEED_VALUE 0xDEAD


extern const unsigned char ucPICAppsectorFirmware[];


