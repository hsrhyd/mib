////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////    usb_mib.c
////
////        USB Driver for Master PIC on MIB Board
////
////                    Mohammad Jamal Mohiuddin
////                    NCR, Inc
////                    07/19/2018
////
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/module.h>
#include <linux/usb.h>
#include <linux/errno.h>
#include <linux/kref.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include "common_interface_comm.h"
#include "usb_mib.h"
#include "panther_microcontroller_requests.h"
#include "POSDeviceAPI.h"
#include "pic_firmware.h"
#include "PIC_Access_Macros.h"
#include "piclet_firmware.h"

#define DRIVER_VERSION  "v0.1"
#define DRIVER_AUTHOR   "Mohammad Jamal Mohiuddin <mm185399@ncr.com>"
#define DRIVER_DESCRIPTION  "USB Driver for MIB Panther Board"
#define MIB_MINOR_BASE  185
#define USB_MIB_VENDOR_ID   0x07d5
#define USB_MIB_PRODUCT_ID  0x04ca
#define RADPC_USB_XFER_TIMEOUT_MS  2
#define to_mib_dev(d) container_of(d, struct mib_device, ref_count)
#define MAX_EMAIL_BYTES 1024

static int min_interrupt_in_interval = RADPC_USB_XFER_TIMEOUT_MS;
static int min_interrupt_out_interval = RADPC_USB_XFER_TIMEOUT_MS;
static struct usb_driver mib_driver;

/* Structure to hold our device specific stuff */
struct mib_device {
    struct mutex mutex;
    struct mutex command_mutex;
    struct usb_device *usb_dev;
    struct usb_interface *interface;
    int error;
    struct usb_endpoint_descriptor *interrupt_in_endpoint;
    int interrupt_in_interval;
    int interrupt_in_endpoint_size;
    int interrupt_in_running;
    int interrupt_in_done;
    struct urb* interrupt_in_urb;
    char *interrupt_in_buffer;
    int bulk_in_received;
    wait_queue_head_t read_wait;
    wait_queue_head_t write_wait;
    struct usb_endpoint_descriptor *interrupt_out_endpoint;
    struct urb* interrupt_out_urb;
    int interrupt_out_interval;
    int interrupt_out_endpoint_size;
    int interrupt_out_busy;
    char *interrupt_out_buffer;
    int bulk_out_transmitted;
    struct task_struct *email_thread;
    struct email_port_data email_port_data;
    struct kref ref_count;
};

static int cf_send_protocol_command(struct mib_device *dev,
                unsigned char command,
                void *request_buffer,
                unsigned long request_buffer_length,
                void *response_buffer,
                unsigned long response_buffer_length,
                unsigned char *protocol_error_code);
static void check_and_update_piclets_app_sector(struct mib_device *dev);

/*++

    mib_usb_abort_transfers

Routine Description:

    Aborts Transfers and Frees associated data structures

Arguments:

    dev - Pointer to device context

--*/
static void mib_usb_abort_transfers(struct mib_device *dev)
{
    if (dev->interrupt_in_running) {
        dev->interrupt_in_running = 0;
        if (dev->interface)
            usb_kill_urb(dev->interrupt_in_urb);
    }
    if (dev->interrupt_out_busy)
        if (dev->interface)
            usb_kill_urb(dev->interrupt_out_urb);
}

/*++

    email_port_read_thread

Routine Description:

    This routine continuously polls the microcontroller for email port data
    because the microcontroller only has enough memory to store 64 bytes of
    data. Calls to read email data simply read data out of the buffer fed by
    this thread.

Arguments:

    data - Pointer to the device's context

Return Value:

    Always zero

--*/

static int email_port_read_thread(void *data)
{
    struct mib_device *dev = (struct mib_device *)data;
    struct in_request_email_rx_data email_rx_data;
    unsigned char protocol_error_code;
    int i;
    int retval = -EINVAL;
    int timeout;
    int retry_count = 0;
    int error_print_count = 0;
    const int fast_timeout = 1;
    const int slow_timeout = 25;
    const int fast_retry_count = 25;
    const int max_error_print_count = 2;

    dev_info(&dev->interface->dev,"%s: Starting kernel thread\n",
            __func__);

    if (!data)
        return retval;

    timeout = slow_timeout;
    while(!kthread_should_stop()) {
        msleep(timeout);
        if (kthread_should_stop())
            break;
        
        retval = cf_send_protocol_command(dev,
                                          PANTH_EMAIL_RX_DATA,
                                          NULL,
                                          0,
                                          &email_rx_data,
                                          sizeof(email_rx_data),
                                          &protocol_error_code);
        if (retval) {
            // Prevent spam by only printing this message
            // cdwMaxErrorPrintCount
            // times without having a successful receive in between
            if (error_print_count != max_error_print_count) {

                dev_err(&dev->interface->dev,"%s: Call to "
                        "cf_send_protocol_command Failed with"
                        "status: (0x%x, 0x%x)\n",
                        __func__, retval, protocol_error_code);
                error_print_count++;
            }
        }
        else if (email_rx_data.receive_len) {
            dev_info(&dev->interface->dev,"%s: Bytes received:"
                    "%d\n", __func__, email_rx_data.receive_len);
            mutex_lock(&dev->email_port_data.data_lock);
            for (i=0;i< email_rx_data.receive_len;i++) {
                dev->email_port_data.read_data[dev->email_port_data.data_index_high]
                    =email_rx_data.rx_data[i];
                dev_info(&dev->interface->dev, "Character[%d]:%c:\t%d\n",
                         i, email_rx_data.rx_data[i], email_rx_data.rx_data[i]);
                dev->email_port_data.data_index_high =
                    (dev->email_port_data.data_index_high+1)%sizeof(dev->email_port_data.read_data);
            }
            mutex_unlock(&dev->email_port_data.data_lock);
            timeout = fast_timeout;
            retry_count = 0;
            error_print_count = 0;
        }
        else if (retry_count != fast_retry_count) {
            retry_count++;
            if (retry_count == fast_retry_count)
                timeout = slow_timeout;
        }
    }
    dev_info(&dev->interface->dev,"%s: Stopping kernel thread\n", __func__);
    return 0;
}

/*++

    read_email_port

Routine Description:

    Reads from the EMAIL Port on the MIB device

Arguments:

    dev - pointer to the device's context
    read_email_data - pointer to an struct piclet_email_port_data
        structure to be filled with data. Before calling this routine, the
        caller must specify the maximum number of bytes to read

Return Value:

    0 on success, error code on Failure

--*/

static int read_email_port(struct mib_device *dev,
                         struct piclet_email_port_data *read_email_data)
{
    int bytes_to_read;
    int bytes_to_avail;
    int i;

    //  Fill the callers buffer with a number of bytes equal to the smaller of
    //  the two following values:
    //  1. The callers buffer size - read_email_data->num_size
    //  2. The number of bytes in the read array
    //  dev->email_port_data.read_data

    mutex_lock(&dev->email_port_data.data_lock);
    // Determine number of bytes available in the ring buffer
    if (dev->email_port_data.data_index_high >= dev->email_port_data.data_index_low)
        bytes_to_avail = dev->email_port_data.data_index_high - dev->email_port_data.data_index_low;
    else
        bytes_to_avail = sizeof(dev->email_port_data.read_data) - (dev->email_port_data.data_index_low - dev->email_port_data.data_index_high);

    // Now take the smaller of the two values
    bytes_to_read = ((read_email_data->num_bytes)>bytes_to_avail)?bytes_to_avail:(read_email_data->num_bytes);
    dev_info(&dev->interface->dev, "%s: bytes_to_read:%d\n", __func__, bytes_to_read);
    // Fill in the data
    for (i = 0;i < bytes_to_read;i++) {
        read_email_data->data[i] =
            dev->email_port_data.read_data[dev->email_port_data.data_index_low];
        dev_info(&dev->interface->dev, "%s: byte[%d]:%c\n", __func__, 
                    i, read_email_data->data[i]);
        dev->email_port_data.data_index_low =
            (dev->email_port_data.data_index_low+1)%sizeof(dev->email_port_data.read_data);
    }

    // Fill in the bytes read
    read_email_data->num_bytes = bytes_to_read;
    mutex_unlock(&dev->email_port_data.data_lock);
    return 0;
}

/*++

    write_piclet_flash_block

Routine Description:

    Writes 32 bytes of Flash Block at the flash address of a particular piclet

Arguments:

    dev - Pointer to device's context
    write_piclet_flash_data - Pointer containing the data to
    write, flash address and piclet address

Return Value:

    0 on success, error code on failure
 
--*/

static int write_piclet_flash_block(struct mib_device *dev,
            struct out_request_write_piclet_flash_data *write_piclet_flash_data)
{
    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev)
        return retval;

    retval = cf_send_protocol_command(dev,
                                      PANTH_WRITE_PICLET_FLASH,
                                      write_piclet_flash_data,
                                      sizeof(*write_piclet_flash_data),
                                      NULL,
                                      0,
                                      &protocol_error_code);

    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;

}

/*++

    read_piclet_flash_block

Routine Description:

     Reads the Flash Address of a particular piclet

Arguments:

    dev - Pointer to device context
    out_read_piclet_flash_data - Pointer to
      struct out_request_read_piclet_flash_data
    in_read_piclet_flash_data - Pointer to
      struct  in_request_read_piclet_flash data where 
        the retrieved flash data will be stored on success

Return Value:
    
    Zero on Success, error code on failure

--*/

static int read_piclet_flash_block(struct mib_device *dev,
        struct out_request_read_piclet_flash_data *out_read_piclet_flash_data,
        struct in_request_read_piclet_flash_data *in_read_piclet_flash_data)
{
    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev || !out_read_piclet_flash_data || !in_read_piclet_flash_data)
        return retval;

    memset(out_read_piclet_flash_data, 0, sizeof(*out_read_piclet_flash_data));
    retval = cf_send_protocol_command(dev,
                                      PANTH_READ_PICLET_FLASH,
                                      out_read_piclet_flash_data,
                                      sizeof(*out_read_piclet_flash_data),
                                      in_read_piclet_flash_data,
                                      sizeof(*in_read_piclet_flash_data),
                                      &protocol_error_code);
    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++
    
    reset_piclet

Routine Description:
    Reset the piclet

Arguments:
    dev - pointer to the device's context
    piclet_address - Piclet Address

Return Value:
    0 on success, error on failure

--*/


static int reset_piclet(struct mib_device *dev, unsigned char piclet_address)
{
    struct out_request_reset_piclet_data reset_piclet_data;
    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev)
        return retval;

    reset_piclet_data.piclet_address = piclet_address;
    dev_info(&dev->interface->dev,"%s: Resetting piclet:%02x\n",
            __func__, piclet_address);
    retval = cf_send_protocol_command(dev,
                                      PANTH_RESET_PICLET,
                                      &reset_piclet_data,
                                      sizeof(reset_piclet_data),
                                      NULL,
                                      0,
                                      &protocol_error_code);

    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++

    get_piclet_operating_mode

Routine Description:
    Retrieves the piclet Operating Mode

Arguments:
    dev - pointer to the device's context
    piclet_address - Piclet Address
    operating_mode - Pointer, if successful, the operating mode
        is stored in it

Return Value:
    0 on success, error on failure

--*/
static int get_piclet_operating_mode(struct mib_device *dev,
                                    unsigned char piclet_address,
                                    unsigned char *operating_mode)
{
    int retval = -EINVAL;
    struct out_request_get_piclet_operating_mode out_req_operating_mode;
    struct in_request_get_piclet_operating_mode in_req_operating_mode;
    unsigned char protocol_error_code;

    if (!dev || !operating_mode)
        return retval;

    memset(&in_req_operating_mode, 0, sizeof(in_req_operating_mode));
    memset(&out_req_operating_mode, 0, sizeof(out_req_operating_mode));

    out_req_operating_mode.piclet_address = piclet_address;
    retval = cf_send_protocol_command(dev,
                                      PANTH_GET_PICLET_OPERATING_MODE,
                                      &out_req_operating_mode,
                                      sizeof(out_req_operating_mode),
                                      &in_req_operating_mode,
                                      sizeof(in_req_operating_mode),
                                      &protocol_error_code);
    if (!retval) {
        *operating_mode = in_req_operating_mode.operating_mode;
        dev_info(&dev->interface->dev,"%s: Address:%02x\t Operating Mode:%d\n",
                __func__, piclet_address, in_req_operating_mode.operating_mode);
    }
    else {
        dev_err(&dev->interface->dev,"%s: Address:%02x\t Failed to get"
                " Operating Mode,ProtocolErrorCode:%02x\n",__func__,
                piclet_address, protocol_error_code);
    }
    return retval;
}

/*++
    
    get_piclet_software_revision

Routine Description:
    Retrieves the Piclet Software Revision

Arguments:
    dev - pointer to the device's context
    piclet_address - address of the piclet
    software_revision - pointer, if successful the software Revision
        is stored in it

Return Value:
    0 on success, error on failure

--*/

static int get_piclet_software_revision(struct mib_device *dev,
                                        unsigned char piclet_address,
                                        unsigned short *software_revision)
{
    struct out_request_get_piclet_software_rev out_req_software_rev;
    struct in_request_get_piclet_software_rev in_req_software_rev;
    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev || !software_revision)
        return retval;

    memset(&in_req_software_rev, 0, sizeof(in_req_software_rev));
    memset(&out_req_software_rev, 0, sizeof(out_req_software_rev));

    out_req_software_rev.piclet_address = piclet_address;

    retval = cf_send_protocol_command(dev,
                                      PANTH_GET_PICLET_SOFTWARE_REV,
                                      &out_req_software_rev,
                                      sizeof(out_req_software_rev),
                                      &in_req_software_rev,
                                      sizeof(in_req_software_rev),
                                      &protocol_error_code);
    if (!retval) {
        dev_info(&dev->interface->dev,"%s: Address:%02x\t"
                "Software Revision:%d\n",__func__,
                piclet_address, in_req_software_rev.software_rev);
        *software_revision = in_req_software_rev.software_rev;
    }
    else {
        dev_err(&dev->interface->dev,"%s: Address:%02x\t Failed"
                " to get Software Revision, protocol_error_code:%02x\n",
                __func__, piclet_address, protocol_error_code);
    }
    return retval;
}

/*++
    
    piclet_jump_to_bsect

Routine Description:
    Jumps to Boot Sector Mode for the passed piclet

Arguments:
    dev - pointer to the device's context
    piclet_address - the piclet address

Return Value:
    0 on success, error on failure

--*/
static int piclet_jump_to_bsect(struct mib_device *dev,
                                unsigned char piclet_address)
{
    unsigned char protocol_error_code;
    struct out_request_piclet_jump_bootsect out_request;
    int retval = -EINVAL;

    if (!dev)
        return retval;

    dev_info(&dev->interface->dev,"%s: piclet_address:%02x\n",
            __func__, piclet_address);

    out_request.piclet_address = piclet_address;

    retval = cf_send_protocol_command(dev,
                                      PANTH_PICLET_JUMP_TO_BOOTSECT,
                                      &out_request,
                                      sizeof(out_request),
                                      NULL,
                                      0,
                                      &protocol_error_code);

    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " Failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++
    
    get_app_revision_usb_request

Routine Description:
    Get the pic Appsector Version

Arguments:
    dev - pointer to the device's context
    app_sector_revision - on success the appsector revision is stored in it

Return Value:
    0 on success, error on failure

--*/
static int get_app_revision_usb_request(struct mib_device *dev,
                                        unsigned short *app_sector_revision)
{

    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev || !app_sector_revision)
        return retval;

    retval = cf_send_protocol_command(dev,
                                      PANTH_GET_APPREVISION,
                                      NULL,
                                      0,
                                      app_sector_revision,
                                      sizeof(*app_sector_revision),
                                      &protocol_error_code);
    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " Failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;

}

/*++

    mib_delete

Routine Description:

    Frees all the data structures allocated and stops the email thread

Arguments:

    ref_count - Reference counter of the device context

--*/

static void mib_delete(struct kref *ref_count)
{
    int ret;
    struct mib_device *dev = to_mib_dev(ref_count);
    mib_usb_abort_transfers(dev);
    usb_free_urb(dev->interrupt_in_urb);
    usb_free_urb(dev->interrupt_out_urb);
    kfree(dev->interrupt_in_buffer);
    kfree(dev->interrupt_out_buffer);
    ret = kthread_stop(dev->email_thread);
    dev_info(&dev->interface->dev, "%s:kthread finish with status:%d\n",
            __func__, ret);
    kfree(dev);
}

/*++
 
    get_mib_board_location

Routine Description:

    Retrieves the MIB Board Location ( 0 or 1 )

Arguments:

    dev - pointer to device context
    board_location - MIB Board location is stored on success

Return Value:

    zero on success, error code on failure

--*/
int get_mib_board_location(struct mib_device *dev,
                           unsigned char *board_location)
{
    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev || !board_location)
        return retval;

    retval =  cf_send_protocol_command(dev,
                                       PANTH_GET_BOARD_LOCATION,
                                       NULL,
                                       0,
                                       board_location,
                                       sizeof(*board_location),
                                       &protocol_error_code);
    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " Failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++
 
    set_485_port_mode

Routine Description:

    Configures the RS485 settings of a piclet

Arguments:

    dev - pointer to device context
    piclet_mode - Pointer to struct piclet_485_mode_opts datastructure

Return Value:

    zero on success, error code on failure

--*/

int set_485_port_mode(struct mib_device *dev,
                      struct piclet_485_mode_opts *piclet_mode)
{
    unsigned char protocol_error_code;
    struct out_request_set_piclet_485_mode out_request;
    int retval = -EINVAL;

    if (!dev || !piclet_mode)
        return retval;

    out_request.piclet_address = piclet_mode->piclet_address;
    out_request.baud_rate = piclet_mode->baud_rate;

    retval = cf_send_protocol_command(dev,
                                      PANTH_SET_PICLET_485_PORT_MODE,
                                      &out_request,
                                      sizeof(out_request),
                                      NULL,
                                      0,
                                      &protocol_error_code);
    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " Failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++

    set_curloop_port_mode

Routine Description:

    Configures the Current Loop settings of a piclet

Arguments:

    dev - pointer to device context
    piclet_mode - Pointer to struct piclet_curloop_mode_opts datastructure

Return Value:

    zero on success, error code on failure

--*/

int set_curloop_port_mode(struct mib_device *dev,
                          struct piclet_curloop_mode_opts *piclet_mode)
{
    unsigned char protocol_error_code;
    struct out_request_set_piclet_curloop_mode out_request;
    int retval = -EINVAL;

    if (!dev || !piclet_mode)
        return retval;

    out_request.piclet_address = piclet_mode->piclet_address;
    out_request.current_loop_mode = piclet_mode->current_loop_mode;
    out_request.current_level= piclet_mode->current_level;
    retval = cf_send_protocol_command(dev,
                                      PANTH_SET_PICLET_CURLOOP_PORT_MODE,
                                      &out_request,
                                      sizeof(out_request),
                                      NULL,
                                      0,
                                      &protocol_error_code);

    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " Failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++

    write_email_port

Routine Description:

    Write the email Data on the EMAIL Port

Arguments:

    dev - pointer to the device's context
    email_data - pointer to struct piclet_email_port_data structure
                containing data to write to the email port

Return Value:

    zero on success, error code on failure

--*/

int write_email_port(struct mib_device *dev,
                     struct piclet_email_port_data *email_data)
{
    unsigned char protocol_error_code;
    struct out_request_email_tx_data out_request;
    unsigned long bytes_to_write;
    unsigned long bytes_written = 0;
    int retval = -EINVAL;
    int i;
    if (!dev || !email_data || !(email_data->data))
        return retval;

    memset(&out_request, 0, sizeof(out_request));
    bytes_to_write = email_data->num_bytes;
    while (bytes_to_write) {
        if (bytes_to_write >= sizeof(out_request.tx_data))
            out_request.transmit_len = sizeof(out_request.tx_data);
        else
            out_request.transmit_len = (unsigned char)bytes_to_write;

        memcpy(out_request.tx_data, email_data->data + bytes_written,
                out_request.transmit_len);
        for ( i =0; i < out_request.transmit_len; i++)
            dev_info(&dev->interface->dev, "%s: byte[%d]:%c\n",
                        __func__, i, out_request.tx_data[i]);
        retval = cf_send_protocol_command(dev,
                                          PANTH_EMAIL_TX_DATA,
                                          &out_request,
                                          sizeof(out_request),
                                          NULL,
                                          0,
                                          &protocol_error_code);
        if (retval) {
            if (RADPC_USBREQUEST_BUFFER_FULL != protocol_error_code) {
                dev_err(&dev->interface->dev,"%s:Call to cf_send_protocol_"
                        "command failed with status: (0x%x, 0x%x)\n",
                        __func__, retval, protocol_error_code);
                break;
            }
        }
        else {
            bytes_written += out_request.transmit_len;
            bytes_to_write -= out_request.transmit_len;
        }
    }
    return retval;
}

/*++

    queue_init_email_port

Routine Description:

    Queues a transmision of pump initialization sequence to be sent
    immediately before the next data transmit

Arguments:

    dev - pointer to the device's context

Return Value:

    0 on success, error code on failure

--*/

int queue_init_email_port(struct mib_device *dev)
{
    unsigned char protocol_error_code;
    int retval = -EINVAL;

    if (!dev)
        return retval;

    retval = cf_send_protocol_command(dev,
                                      PANTH_EMAIL_QUEUE_INIT,
                                      NULL,
                                      0,
                                      NULL,
                                      0,
                                      &protocol_error_code);
    if (retval)
        dev_err(&dev->interface->dev, "%s:Call to cf_send_protocol_command"
                " failed with status: (0x%x, 0x%x)\n",__func__, retval,
                protocol_error_code);
    return retval;
}

/*++

    issue_dfu_detach_usb_req

Routine Description:

    Issues DFU_DETACH class request to PIC, which should
    initiate an appsector download

Arguments:

    dev - pointer to the device's context

Return Value:

    0 on success, error value on failure

--*/
static int issue_dfu_detach_usb_req(struct mib_device *dev)
{
    int retval = -EINVAL;
    
    if (!dev)
        return retval;

    retval = usb_control_msg(dev->usb_dev,
                             usb_sndctrlpipe(dev->usb_dev, 0),
                             DFU_DETACH,
                             USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_ENDPOINT,
                             0,
                             0,
                             NULL,
                             0,
                             USB_CTRL_SET_TIMEOUT);
    if (retval)
        dev_info(&dev->interface->dev, "%s: retval:%d\n", __func__, retval);
    return retval;

}

/*++

    check_and_update_piclets_app_sector

Routine Description:

    Checks the software revision on the piclets with the current version
    and flashes if different

Arguments:

    dev - Pointer to device's context

--*/

static void check_and_update_piclets_app_sector(struct mib_device *dev)
{
    unsigned long start_time, end_time;
    unsigned char piclet_address = 0;
    unsigned char operating_mode = 0xFF;
    unsigned short software_revision = 0;
    unsigned int i = 0;
    struct out_request_write_piclet_flash_data out_write_flash_data;

    start_time = jiffies;

    for (i = 0; i < 20; i++) {
        if ((16 == i) || (17 == i))
            // There are no PIClets at these addresses
            continue;
        software_revision = 0xFFFF;
        piclet_address = (0x80 + 2*i);

        if (get_piclet_operating_mode(dev, piclet_address, &operating_mode)) {
            dev_warn(&dev->interface->dev, "%s:get_piclet_operating_mode on"
                    " 0x%x failed \n", __func__, piclet_address);
            continue;
        }
        //Operating in Appsector Mode
        if (1 == operating_mode) {
            if (get_piclet_software_revision(dev, piclet_address,
                        &software_revision)) {
                dev_warn(&dev->interface->dev, "%s:get_piclet_software_revision"
                        " on 0x%x failed\n", __func__, piclet_address);
                continue;
            }

            if (PICLET_APPSECTOR_CURRENT_VERSION > software_revision) {
                dev_info(&dev->interface->dev, "%s:Piclet at address 0x%x"
                        " requires appsector update\n", __func__,
                        piclet_address);
                if (piclet_jump_to_bsect(dev, piclet_address)) {
                    dev_warn(&dev->interface->dev,"%s:piclet_jump_to_bsect "
                            "failed on 0x%x\n", __func__, piclet_address);
                    continue;
                }
                //Give the piclet sometime to come back up
                msleep(10);
                operating_mode = 0;
            }
        }
        // Now we are in boot sector mode
        if (0 == operating_mode) {
            int index;
            dev_info(&dev->interface->dev, "%s: Updating PIClet at address 0x%x"
                    " from app revision 0x%x to 0x%x\n",__func__,
                    piclet_address, software_revision,
                    PICLET_APPSECTOR_CURRENT_VERSION);

            out_write_flash_data.piclet_address = piclet_address;
            for (index = 0; index < PICLET_APPSECTOR_SIZE; index += 32) {
                out_write_flash_data.flash_address =
                     (unsigned short)((PICLET_APPSECTOR_START_ADDRESS+index)/2);

                memcpy(out_write_flash_data.flash_data,
                        ucPICletAppsectorFirmware+index, 32);
                if (write_piclet_flash_block(dev, &out_write_flash_data)) {
                    dev_err(&dev->interface->dev,"%s: write_piclet_flash_block "
                            "on 0x%x failed\n", __func__, piclet_address);
                    break;
                }
            }

            if (PICLET_APPSECTOR_SIZE == index) {
                if (reset_piclet(dev, piclet_address)) 
                    dev_err(&dev->interface->dev, "%s: reset_piclet on 0x%x"
                            " failed\n", __func__, piclet_address);
            }
        }
    }
    end_time = jiffies;
    dev_info(&dev->interface->dev, "%s:CheckAndUpdatePicletsAppsector completed"
            " after %ums\n", __func__, jiffies_to_msecs(end_time - start_time));
}

static char *mib_devnode(struct device *dev, umode_t *mode)
{
    return kasprintf(GFP_KERNEL, "usb/%s", dev_name(dev));
}

static int mib_open(struct inode *inode, struct file *file)
{
    int subminor;
    struct mib_device *dev;
    struct usb_interface *interface;
    int retval = 0;

    nonseekable_open(inode, file);
    subminor = iminor(inode);

    interface = usb_find_interface(&mib_driver, subminor);

    if (!interface) {
        printk(KERN_ERR "%s -error, can't find device for minor:%d\n",
                __func__, subminor);
        retval = -ENODEV;
        goto exit;
    }

    dev = usb_get_intfdata(interface);

    if (!dev) {
        printk(KERN_ERR "%s Failed to get interface data\n", __func__);
        retval = -ENODEV;
        goto exit;
    }

    /*Increment our usage count for the device*/
    kref_get(&dev->ref_count);

    /*Save device in the file's private data */
    file->private_data = dev;

exit:
    return retval;
}

static int mib_release(struct inode *inode, struct file *file)
{
    struct mib_device *dev;
    int retval = 0;

    dev = file->private_data;

    if (dev == NULL) {
        retval = -ENODEV;
        goto exit;
    }
    /* decrement the count on our device */
    kref_put(&dev->ref_count, mib_delete);
exit:
    if (mutex_is_locked(&dev->mutex))
        mutex_unlock(&dev->mutex);
    return retval;
}

static long mib_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct mib_device *dev;
    int retval = 0;
    long size = _IOC_SIZE(cmd);
    struct piclet_485_mode_opts piclet_485_mode;
    struct piclet_curloop_mode_opts piclet_cur_loop_mode;
    struct usb_info usb_information;
    unsigned char mib_board_location = 255;

    if (_IOC_DIR(cmd) & _IOC_READ)
        retval = !access_ok(VERIFY_WRITE, (void __user *)arg, size);
    else if (_IOC_DIR(cmd) & _IOC_WRITE)
        retval = !access_ok(VERIFY_READ, (void __user *)arg, size);

    if (retval)
        return -EFAULT;

    dev = file->private_data;
    if (dev == NULL) {
        retval = -ENODEV;
        goto exit;
    }
    mutex_lock_interruptible(&dev->mutex);
    switch(cmd)
    {
        case GEN_IOCTL_GET_MIB_BOARD_LOCATION:
        {
            dev_info(&dev->interface->dev, "%s:"
                    "GEN_IOCTL_GET_MIB_BOARD_LOCATION\n", __func__);
            get_mib_board_location(dev, &mib_board_location);
            dev_info(&dev->interface->dev, "%s: MIB Board Location:%d\n",
                    __func__, mib_board_location);
            retval = put_user(mib_board_location,(unsigned char __user *)arg);
        }
        break;

        case GEN_IOCTL_SET_PICLET_485_PORT_MODE:
        {
            dev_info(&dev->interface->dev, "%s:"
                     "GEN_IOCTL_SET_PICLET_485_PORT_MODE\n", __func__);
            retval = copy_from_user(&piclet_485_mode,
                                    (struct piclet_485_mode_opts *)arg,
                                    sizeof(struct piclet_485_mode_opts));
            if (!retval)
                retval = set_485_port_mode(dev, &piclet_485_mode);
            else
                retval = -EFAULT;
        }
        break;

        case GEN_IOCTL_SET_PICLET_CURLOOP_PORT_MODE:
        {
            dev_info(&dev->interface->dev, "%s:"
                    "GEN_IOCTL_SET_PICLET_CURLOOP_PORT_MODE\n", __func__);
            retval = copy_from_user(&piclet_cur_loop_mode,
                                    (struct piclet_curloop_mode_opts *)arg,
                                    sizeof(struct piclet_curloop_mode_opts));
            if (!retval)
                retval = set_curloop_port_mode(dev, &piclet_cur_loop_mode);
            else
                retval = -EFAULT;
        }
        break;

        case GEN_IOCTL_GET_APPSECTOR_REVISION:
        {
            unsigned short app_revision;
            dev_info(&dev->interface->dev, "%s:"
                    "GEN_IOCTL_APPSECTOR_REVISION\n", __func__);
            retval = get_app_revision_usb_request(dev, &app_revision);
            if (!retval) {
                dev_info(&dev->interface->dev, "%s: revision:%02x\n",
                        __func__, app_revision);
                retval = copy_to_user((unsigned short *)arg,
                                      &app_revision, sizeof(unsigned short));
                if (retval)
                        retval = -EFAULT;
            }
        }
        break;

        case GEN_IOCTL_EMAIL_PORT_WRITE:
        {
            struct piclet_email_port_data email_port_data;
            memset(&email_port_data, 0, sizeof(email_port_data));
            dev_info(&dev->interface->dev, "%s:"
                    "GEN_IOCTL_EMAIL_PORT_WRITE\n", __func__);
            email_port_data.num_bytes = strnlen_user((unsigned char *)arg,
                                                      MAX_EMAIL_BYTES);
            if (!email_port_data.num_bytes) {
                dev_err(&dev->interface->dev, "%s: strnlen_user failed\n",
                        __func__);
                retval = -EINVAL;
            }
            else {
                retval = copy_from_user(email_port_data.data,
                                        (unsigned char *)arg,
                                        email_port_data.num_bytes);
                if (!retval)
                    retval = write_email_port(dev, &email_port_data);
                else
                    retval = -EFAULT;
            }
        }
        break;

        case GEN_IOCTL_EMAIL_PORT_QUEUE_INIT:
        {
            dev_info(&dev->interface->dev, "%s:"
                    "GEN_IOCTL_EMAIL_PORT_QUEUE_INIT\n", __func__);
            retval = queue_init_email_port(dev);
        }
        break;

        case GEN_IOCTL_EMAIL_PORT_READ:
        {
            struct piclet_email_port_data email_port_data;
            memset(&email_port_data, 0, sizeof(email_port_data));
            dev_info(&dev->interface->dev, "%s:"
                     "GEN_IOCTL_EMAIL_PORT_READ\n", __func__);
            retval = copy_from_user(&email_port_data.num_bytes,
                                    &((struct piclet_email_port_data *)arg)->num_bytes,
                                    sizeof(email_port_data.num_bytes));
            if (!retval) {
                dev_info(&dev->interface->dev, "%s: Requested:%d"
                        "bytes\n", __func__, email_port_data.num_bytes);
                retval = read_email_port(dev, &email_port_data);
                dev_info(&dev->interface->dev, "%s: read_email_port ret:%d\n",
                        __func__, retval);
                if (!retval) {
                    retval = copy_to_user(((struct piclet_email_port_data *)arg),
                                          &email_port_data,
                                          sizeof(email_port_data));
                }  
            }
            else {
                retval = -EFAULT;
            }
        }
        break;

        case PANTH_IOCTL_GET_USB_DEV_INFO:
            dev_info(&dev->interface->dev, "%s:"
                     "PANTH_IOCTL_GET_USB_NUMBER\n", __func__);
            dev_info(&dev->interface->dev, "%s:"
                     "parent device number:%d\n", 
                     __func__, dev->usb_dev->parent->devnum);
            usb_information.parent_dev_num = dev->usb_dev->parent->devnum;
            dev_info(&dev->interface->dev, "%s:"
                     "bus number:%d\n", __func__, dev->usb_dev->bus->busnum);
            usb_information.bus_number = dev->usb_dev->bus->busnum;
            retval = copy_to_user((struct usb_info *)arg,
                                   &usb_information,
                                   sizeof(struct usb_info));
            if (retval)
                retval = -EFAULT;
        break;

        default:
        {
            dev_warn(&dev->interface->dev, "%s:Unknown ioctl command:%d\n",
                    __func__, cmd);
            retval = -EOPNOTSUPP;
        }
        break;
    }
    mutex_unlock(&dev->mutex);
exit:
    return retval;
}

//read, write and seek are not supported as in WinCE Code
static const struct file_operations mib_fops = {
    .owner = THIS_MODULE,
    .open = mib_open,
    .release = mib_release,
    .llseek = NULL,
    .read = NULL,
    .write = NULL, 
    .unlocked_ioctl = mib_ioctl,
};

static struct usb_class_driver mib_class = {
    .name = "mib%d",
    .devnode = mib_devnode,
    .fops = &mib_fops,
    .minor_base = MIB_MINOR_BASE,
};

static const struct usb_device_id mib_table[] = {
    { USB_DEVICE(USB_MIB_VENDOR_ID, USB_MIB_PRODUCT_ID) },
    { }
};

MODULE_DEVICE_TABLE(usb, mib_table);

static void mib_usb_interrupt_out_callback(struct urb *urb)
{
    struct mib_device *dev = urb->context;
    if (urb->status)
        dev->error = urb->status;
    else
        dev->bulk_out_transmitted = urb->actual_length;
    dev->interrupt_out_busy = 0;
    wake_up_interruptible(&dev->write_wait);

}

static void mib_usb_interrupt_in_callback(struct urb *urb)
{
    struct mib_device *dev = urb->context;
    if (urb->status)
        dev->error = urb->status;
    else
        dev->bulk_in_received = urb->actual_length;
    dev->interrupt_in_done = 1;
    wake_up_interruptible(&dev->read_wait);
}

/*++

    flush_pipe_in_buffer

Routine Description:
 
    Flushes the incoming buffer of any response

Arguments:

    dev - Pointer to device context

--*/
static void flush_pipe_in_buffer(struct mib_device *dev)
{
    int retval;
    memset(dev->interrupt_in_buffer, 0, dev->interrupt_in_endpoint_size);
    usb_fill_int_urb(dev->interrupt_in_urb,
            dev->usb_dev,
            usb_rcvintpipe(dev->usb_dev,
                dev->interrupt_in_endpoint->bEndpointAddress),
            dev->interrupt_in_buffer,
            dev->interrupt_in_endpoint_size,
            mib_usb_interrupt_in_callback,
            dev,
            dev->interrupt_in_interval);

    dev->interrupt_in_running = 1;
    dev->interrupt_in_done = 0;

    retval = usb_submit_urb(dev->interrupt_in_urb, GFP_KERNEL);
    if (retval) {
        dev_err(&dev->interface->dev, "%s:Failed to submit IN urb,"
                "retval:%d\n", __func__, retval);
        return;
    }

    retval = wait_event_interruptible(dev->read_wait, dev->interrupt_in_done);

    if (retval)
        return;
    if (dev->error) {
        retval = dev->error;
        dev_err(&dev->interface->dev, "%s:Error:%d after Transmitting IN URB\n"
                , __func__, retval);
    }
}


/*++

    cf_send_protocol_command

Routine Description:
    Transmits a command and receives a response from the microcontroller

Arguments:
    
    dev - Pointer to the device context
    command - Command for the microcontroller
    request_buffer - Input buffer, if applicable
    request_buffer_length - Input buffer size, in bytes
    response_buffer - Output buffer, if applicable
    response_buffer_length - Output buffer size, in bytes
    protocol_error_code   - RADPC_USBREQUEST_NO_ERROR on success, other USB
        protocol error if the call failed due to usb error

Return Value:

    zero on success, error code on failure

--*/

int cf_send_protocol_command(struct mib_device *dev,
        unsigned char command,
        void *request_buffer,
        unsigned long request_buffer_length,
        void *response_buffer,
        unsigned long response_buffer_length,
        unsigned char *protocol_error_code)
{

    int retval = -EINVAL;
    int i;
    int buffer_index;
    unsigned short request_id = (unsigned short)(jiffies & 0xFFFF);

    if (protocol_error_code)
        *protocol_error_code = RADPC_USBREQUEST_UNDEFINED;

    if ((request_buffer_length && !request_buffer) ||
         (response_buffer_length && !response_buffer)) {
        dev_err(&dev->interface->dev,"%s: Invalid Parameter,"
                "pointer found %p %p \n",__func__, request_buffer,
                response_buffer);
        goto lbl_cf_send_protocol_command_end;
    }

    if (request_buffer_length + sizeof(struct out_request_header) >
                    dev->interrupt_out_endpoint_size) {
        dev_err(&dev->interface->dev, "%s: Out buffer length too large"
                " for endpoint found:%lu expected:%u\n", __func__,
                request_buffer_length + sizeof(struct out_request_header),
                dev->interrupt_out_endpoint_size);
        goto lbl_cf_send_protocol_command_end;
    }

    if (response_buffer_length + sizeof(struct in_request_header) >
                    dev->interrupt_in_endpoint_size) {

        dev_err(&dev->interface->dev, "%s: In buffer length too large"
                " for endpoint found:%lu expected:%u\n", __func__,
                response_buffer_length + sizeof(struct in_request_header),
                dev->interrupt_in_endpoint_size);
        goto lbl_cf_send_protocol_command_end;
    }

    mutex_lock_interruptible(&dev->command_mutex);
    memset(dev->interrupt_out_buffer, 0, dev->interrupt_out_endpoint_size);
    //
    //  |===========================================================
    //  |  RequestId  ||  RequestId  ||  Command    ||  Data...    |
    //  |   Lsb       ||   Msb       ||             ||             |
    //  |===========================================================
    //
    ((struct out_request_header *)dev->interrupt_out_buffer)->request_id = request_id;
    ((struct out_request_header *)dev->interrupt_out_buffer)->command = command;
    buffer_index = sizeof(struct out_request_header);
    for (i = 0; (i < request_buffer_length) &&
                    (buffer_index < dev->interrupt_out_endpoint_size-1);)
        dev->interrupt_out_buffer[buffer_index++] =
                ((unsigned char *)request_buffer)[i++];

    usb_fill_int_urb(dev->interrupt_out_urb,
            dev->usb_dev,
            usb_sndintpipe(dev->usb_dev,
                dev->interrupt_out_endpoint->bEndpointAddress),
            dev->interrupt_out_buffer,
            buffer_index,
            mib_usb_interrupt_out_callback,
            dev,
            dev->interrupt_out_interval);

    dev->interrupt_out_busy = 1;
    retval = usb_submit_urb(dev->interrupt_out_urb, GFP_KERNEL);

    if (retval) {
        dev_err(&dev->interface->dev, "%s: Error:%d while submitting OUT URB\n",
                __func__, retval);
        goto lbl_cf_send_protocol_command_end;
    }

    retval = wait_event_interruptible(dev->write_wait, !dev->interrupt_out_busy);

    if (retval < 0)
        goto lbl_cf_send_protocol_command_end;

    if (dev->error) {
        retval = dev->error;
        dev_err(&dev->interface->dev, "%s:Error:%d after Transmitting OUT URB\n"
                , __func__, retval);
        goto lbl_cf_send_protocol_command_end;
    }

    //
    // Do a check on the bytes written, not sure if there is much of a way
    // for this to fail and the write to succeed
    //
    if (buffer_index != dev->bulk_out_transmitted) {
        dev_err(&dev->interface->dev, "%s: Bytes Written %02x does not match"
                " requested bytes written:%02x\n",__func__, buffer_index,
                dev->bulk_out_transmitted);
        retval = -EINVAL;
        goto lbl_cf_send_protocol_command_end;

    }

    ///////////////////////////////////////////////////////////////////////////
    // Now let's get the response
    ///////////////////////////////////////////////////////////////////////////

    memset(dev->interrupt_in_buffer, 0, dev->interrupt_in_endpoint_size);

    usb_fill_int_urb(dev->interrupt_in_urb,
            dev->usb_dev,
            usb_rcvintpipe(dev->usb_dev,
                dev->interrupt_in_endpoint->bEndpointAddress),
            dev->interrupt_in_buffer,
            dev->interrupt_in_endpoint_size,
            mib_usb_interrupt_in_callback,
            dev,
            dev->interrupt_in_interval);

    dev->interrupt_in_running = 1;
    dev->interrupt_in_done = 0;

    retval = usb_submit_urb(dev->interrupt_in_urb, GFP_KERNEL);
    if (retval) {
        dev_err(&dev->interface->dev, "%s:Failed to submit IN urb,"
                "retval:%d\n", __func__, retval);
        goto lbl_cf_send_protocol_command_end;
    }

    retval = wait_event_interruptible(dev->read_wait, dev->interrupt_in_done);

    if (retval)
        goto lbl_cf_send_protocol_command_end;

    if (dev->error) {
        retval = dev->error;
        dev_err(&dev->interface->dev, "%s:Error:%d after Transmitting IN URB\n"
                , __func__, retval);
        goto lbl_cf_send_protocol_command_end;
    }

    //
    // First make sure a valid header was returned so that the error code can be
    // examined. Do not check recieved byte length against required byte length
    // first as an error will usually return a smaller size than required.
    //

    if (dev->bulk_in_received < sizeof(struct in_request_header)) {
        dev_err(&dev->interface->dev, "%s: Incomplete header returned, expected"
                " atleast %lu bytes, received:%d bytes\n",
                __func__, sizeof(struct in_request_header),
                dev->bulk_in_received);
        retval = -EINVAL;
        goto lbl_cf_send_protocol_command_end;
    }

    //Make sure this is the right response back
    if (((struct in_request_header *)dev->interrupt_in_buffer)->request_id != request_id)
    {
        dev_err(&dev->interface->dev, "%s: Requested ID transmitted %02x"
                " does not match request id returned:%02x\n", __func__, 
                request_id,
                ((struct in_request_header *)dev->interrupt_in_buffer)->request_id);
        flush_pipe_in_buffer(dev);
        retval = -EINVAL;
        goto lbl_cf_send_protocol_command_end;
    }

    if (protocol_error_code)
        *protocol_error_code =
            ((struct in_request_header *)dev->interrupt_in_buffer)->error_code;

    //
    // Check the error code, data validity can not be assumed until this happens
    //

    if (((struct in_request_header *)dev->interrupt_in_buffer)->error_code
                    != RADPC_USBREQUEST_NO_ERROR) {
        dev_err(&dev->interface->dev, "%s: Request Error, return value=%02x\n",
                __func__, 
                ((struct in_request_header *)dev->interrupt_in_buffer)->error_code);
        retval = -EINVAL;
        goto lbl_cf_send_protocol_command_end;
    }

    //
    // Enforce an exact buffer size where callers of this function must pass
    // in a buffer that is == to size of data returned from micro
    //

    if (dev->bulk_in_received !=
          (sizeof(struct in_request_header) + response_buffer_length))
    {
        dev_err(&dev->interface->dev, "%s:Data size returned %d failed to match"
                " data requested %lu\n", __func__, dev->bulk_in_received,
                sizeof(struct in_request_header) + response_buffer_length);
        retval = -EINVAL;
        goto lbl_cf_send_protocol_command_end;
    }

    for(i = 0; i < response_buffer_length; i++)
        ((unsigned char *)(response_buffer))[i] =
         dev->interrupt_in_buffer[sizeof(struct in_request_header)+i];
    retval = 0;
lbl_cf_send_protocol_command_end:
    if (mutex_is_locked(&dev->command_mutex))
        mutex_unlock(&dev->command_mutex);
    return retval;
}

/*++

    is_general_mib_interface

Routine Description:

    Checks whether the passed interface is general or not

Arguments:

    iface_desc: Interface Descriptor

Return Value:

    Return 1 if interface is general else 0

--*/

int is_general_mib_interface(struct usb_host_interface *iface_desc)
{
    if (((USB_INTERFACE_CLASS_VENDOR_SPECIFIC == iface_desc->desc.bInterfaceClass) &&
        (USB_INTERFACE_SUBCLASS_UNDEFINED == iface_desc->desc.bInterfaceSubClass)) ||
        ((USB_INTERFACE_CLASS_APPLICATION_SPECIFIC_INTERFACE == iface_desc->desc.bInterfaceClass) &&
         (USB_INTERFACE_SUBCLASS_DFU == iface_desc->desc.bInterfaceSubClass))) {
        if ((INTERFACE_MSR != iface_desc->desc.bInterfaceNumber) &&
                (INTERFACE_GENERAL != iface_desc->desc.bInterfaceNumber))
            return 0;
        return 1;
    }
    return 0;
}

static int mib_probe(struct usb_interface *interface,
                     const struct usb_device_id *id)
{
    unsigned int i;
    unsigned int num_endpoints;
    struct usb_endpoint_descriptor *endpoint;
    struct mib_device *dev = NULL;
    struct usb_host_interface *iface_desc = interface->cur_altsetting;
    int retval = -EINVAL;
    unsigned short current_appsector_rev;

    if (!is_general_mib_interface(iface_desc)) {
        dev_info(&interface->dev, "%s: Not MSR or Gen Interface, so hacking out"
                "now [%d] \n",__func__, iface_desc->desc.bInterfaceNumber);
        return -ENODEV;
    }

    //we found a device, interface and protocol, we can control,
    //so create our device context
    dev = kzalloc(sizeof(*dev), GFP_KERNEL);

    if (dev == NULL) {
        dev_err(&interface->dev, "%s:Failed to allocate memory of size:%lu\n",
                __func__, sizeof(*dev));
        return -ENOMEM;
    }
    kref_init(&dev->ref_count);

    mutex_init(&dev->mutex);
    mutex_init(&dev->command_mutex);
    mutex_init(&dev->email_port_data.data_lock);

    init_waitqueue_head(&dev->read_wait);
    init_waitqueue_head(&dev->write_wait);

    dev->interface = interface;
    dev->usb_dev = interface_to_usbdev(interface);
    num_endpoints = interface->cur_altsetting->desc.bNumEndpoints;

    //setup the endpoint information
    for (i = 0; i < num_endpoints; i++) {
        DUMP_USB_ENDPOINT_DESCRIPTOR(iface_desc->endpoint[i].desc);
        endpoint = &iface_desc->endpoint[i].desc;
        if (usb_endpoint_is_int_in(endpoint))
            dev->interrupt_in_endpoint = endpoint;
        if (usb_endpoint_is_int_out(endpoint))
            dev->interrupt_out_endpoint = endpoint;
    }

    if (dev->interrupt_in_endpoint == NULL) {
        dev_err(&interface->dev, "Interrupt in endpoint not found\n");
        goto error;
    }

    if (dev->interrupt_out_endpoint == NULL) {
        dev_err(&interface->dev, "Interrupt out endpoint not found\n");
        goto error;
    }

    dev->interrupt_in_endpoint_size =
            usb_endpoint_maxp(dev->interrupt_in_endpoint);
    dev_info(&interface->dev, "%s:In endpoint buffer size:%d\n",
             __func__, dev->interrupt_in_endpoint_size);
    dev->interrupt_in_buffer =
            kzalloc(dev->interrupt_in_endpoint_size, GFP_KERNEL);
    if (!dev->interrupt_in_buffer) {
        dev_err(&interface->dev, "Couldn't allocate interrupt_in_buffer\n");
        goto error;
    }
    dev->interrupt_in_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (!dev->interrupt_in_urb) {
        dev_err(&interface->dev, "Could not allocate interrupt_in_urb\n");
        goto error;
    }

    dev->interrupt_out_endpoint_size =
            usb_endpoint_maxp(dev->interrupt_out_endpoint);
    dev_info(&interface->dev, "%s: Out endpoint size:%d\n",
             __func__, dev->interrupt_out_endpoint_size);
    dev->interrupt_out_buffer =
            kzalloc(dev->interrupt_out_endpoint_size, GFP_KERNEL);
    if (!dev->interrupt_out_buffer) {
        dev_err(&interface->dev, "Couldn't allocate interrupt_out_buffer\n");
        goto error;
    }

    dev->interrupt_out_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (!dev->interrupt_out_urb) {
        dev_err(&interface->dev, "Could not allocate interrupt_out_urb\n");
        goto error;
    }

    dev->interrupt_in_interval = min_interrupt_in_interval > dev->interrupt_in_endpoint->bInterval ? min_interrupt_in_interval : dev->interrupt_in_endpoint->bInterval;
    dev->interrupt_out_interval = min_interrupt_out_interval > dev->interrupt_out_endpoint->bInterval ? min_interrupt_out_interval : dev->interrupt_out_endpoint->bInterval;

    usb_set_intfdata(interface, dev);
    retval = usb_register_dev(interface, &mib_class);
    if (retval) {
        dev_err(&interface->dev, "Not able to get a minor for this dev\n");
        usb_set_intfdata(interface, NULL);
        goto error;

    }
    check_and_update_piclets_app_sector(dev);
    dev->email_thread = kthread_run(email_port_read_thread, dev, "emailreader");

    //Determine if a new appsector is needed
    if (!get_app_revision_usb_request(dev, &current_appsector_rev)) {
        if (APP_REVISION_PIC == current_appsector_rev)// Revision is good
            dev_info(&dev->interface->dev, "%s: Detected Expected"
                 "Appsector Revision [0x%04x]\n", __func__,
                 current_appsector_rev);
        else {
            // Incorrect revision.  Send "DFU Detach" command to appsector,
            // which will cause PIC to reset with switch
            // to force new appsector download.
            dev_info(&dev->interface->dev, "%s: Unexpected"
                    "Appsector found (0x%04x) !Initiating update"
                    "to revision 0x%04x...\n", __func__,
                    current_appsector_rev, APP_REVISION_PIC);
            retval = issue_dfu_detach_usb_req(dev);
            if (retval) {
                dev_err(&interface->dev, "%s:DFU Detach Usb Request Failed with"
                        "return value:%d\n",__func__, retval);
            }
        }
    }
    return retval;
error:
    if (dev)
        kref_put(&dev->ref_count, mib_delete);
    return retval;
}

static void mib_disconnect(struct usb_interface *interface)
{
    struct mib_device *dev;
    dev = usb_get_intfdata(interface);
    dev_info(&dev->interface->dev, "%s\n", __func__);
    usb_deregister_dev(interface, &mib_class);
    /* Decrement our usage count */
    kref_put(&dev->ref_count, mib_delete);
    usb_set_intfdata(interface, NULL);
}

static struct usb_driver mib_driver = {
    .name = "mib",
    .probe = mib_probe,
    .disconnect = mib_disconnect,
    .id_table = mib_table,
};

module_usb_driver(mib_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESCRIPTION);
MODULE_VERSION(DRIVER_VERSION);
