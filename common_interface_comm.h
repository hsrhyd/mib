////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////
//////    common_interface_comm.h
//////
//////        Error and Structure Definitions
//////
//////                    Mohammad Jamal Mohiuddin
//////                    NCR, Inc
//////                    07/19/2018
//////
//////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */



#ifndef COMMON_INTERFACE_COMM_H
#define COMMON_INTERFACE_COMM_H


//////////////////////////////////////////////////////////
//  Request Error Codes
//////////////////////////////////////////////////////////
#define RADPC_USBREQUEST_NO_ERROR                       0
#define RADPC_USBREQUEST_UNDEFINED                      1
#define RADPC_USBREQUEST_GENERAL_ERROR                  2
#define RADPC_USBREQUEST_INVALID_OUT_HEADER_SIZE        3
#define RADPC_USBREQUEST_INVALID_OUT_PACKET_SIZE        4
#define RADPC_USBREQUEST_INVALID_OUT_REQUEST_TYPE       5
#define RADPC_USBREQUEST_INVALID_IN_REQUEST_TYPE        6
#define RADPC_USBREQUEST_BUFFER_FULL                    7
#define RADPC_USBREQUEST_BUFFER_EMPTY                   8


// avoid printing out the actual error number, print out the string
// instead which should save the debugging effort some time
#define RADPC_USBREQUEST_ERROR_STRING_TABLE {   \
        L"No Error",                            \
        L"Undefined",                           \
        L"General Error",                       \
        L"Invalid Out Header Size",             \
        L"Invalid Out Packet Size",             \
        L"Invalid Out Request Type",            \
        L"Invalid In Request Type",             \
        L"Buffer Full",                         \
        L"Buffer Empty",                        \
        NULL                                    \
        };

struct out_request_header{
    unsigned short request_id;
    unsigned char  command;         
}__attribute__((packed)); 

struct in_request_header {
    unsigned short request_id;
    unsigned char  error_code;
}__attribute__((packed));

struct out_request_email_tx_data {
    unsigned char  transmit_len;
    unsigned char  tx_data[32];
}__attribute__((packed));


struct out_request_set_piclet_485_mode {
    unsigned char   piclet_address;
    unsigned int   baud_rate;      
}__attribute__((packed));

struct out_request_set_piclet_curloop_mode {
    unsigned char   piclet_address;
    unsigned char   current_loop_mode;    
    unsigned char   current_level;      
}__attribute__((packed));

struct out_request_reset_piclet_data {
    unsigned char   piclet_address;
}__attribute__((packed)); 

struct out_request_get_piclet_operating_mode {
    unsigned char   piclet_address;       
}__attribute__((packed));

struct in_request_get_piclet_operating_mode { 
    char    operating_mode;
}__attribute__((packed));

struct out_request_get_piclet_software_rev { 
    unsigned char   piclet_address;       
}__attribute__((packed));


struct in_request_get_piclet_software_rev {
    unsigned short  software_rev;
}__attribute__((packed));


struct out_request_piclet_jump_bootsect {
    unsigned char   piclet_address;
}__attribute__((packed));

struct in_request_email_rx_data {
    unsigned char  receive_len; 
    unsigned char  rx_data[32];
}__attribute__((packed));


struct out_request_write_piclet_flash_data { 
    unsigned char   piclet_address;
    unsigned short  flash_address;
    unsigned char   flash_data[32]; 
}__attribute__((packed));

struct out_request_read_piclet_flash_data {
    unsigned char   piclet_address;
    unsigned short  flash_address; 
}__attribute__((packed));

struct in_request_read_piclet_flash_data {
    unsigned char flashdata[32];
}__attribute__((packed));

#endif
