/*********************************************************************
 * FileName:        panther_microcontroller_requests.h
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18
 * Compiler:        C18 2.30.01+
 * Company:         Radiant Systems, Inc.
 *
 *
 *                      Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Drew Wurfel          3/27/09     Copying requests from DFUAppInfo.h
 *                                  into a header than can be shared 
 *                                  across all microcontrollers
 * Cyle Taylor         04/23/2014   WIT 691889 - Add EMAIL port
 *                                  support
 ********************************************************************/



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//// MICROCONTROLLER REQUESTS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define PANTH_GET_APPSIGNATURE                               0       // get the signature placed on the code in the app
#define PANTH_GET_APPREVISION                                1       // get the revision placed on the code in the app
#define PANTH_GET_APPWORD                                    2       // get a word from the app's program memory (backdoor)
#define PANTH_GET_BOOTLOADREV                                3       // get the revision of the bootloader
#define PANTH_GET_VALIDITY                                   4       // find out whether the FW is valid
#define PANTH_JUMP_APPLICATION                               5       // jump to the application stored in the application sectors
#define PANTH_SWITCH_TO_DFU                                  6       // switch to the DFU from the application

#define PANTH_READ_EEPROM                                    7       // read location from eeprom
#define PANTH_WRITE_EEPROM                                   8       // write location on eeprom
#define PANTH_GET_PLATFORM_TYPE                              9       // get platform type


/********************************************************************************
 *  PANTH_READ_PROFILER_1
 *
 *      Description
 *          Returns microcontroller profiler data such as main loop iterations/s, 
 *          interrupts/s, touchs/s.
 *          
 *      Input   - none
 *      Output  - PROFILER_1_DATA suProfile
 */
#define PANTH_READ_PROFILER_1                                10      // profiler 1, int count, main loop count



/********************************************************************************
 *  PANTH_RESET_MICROCONTROLLER
 *
 *      Description
 *          [DJW 02/09/2012] WIT 662368 - causes the micro to issue a soft reset.
 *          This command is used for testing purposes.
 *
 *      Input   - none
 *      Output  - none
 */
#define PANTH_RESET_MICROCONTROLLER                          11



/********************************************************************************
 *  PANTH_GET_BOARD_REVISION
 *
 *      Description
 *          [DJW 02/09/2012] WIT 662368 - Used to get the board revision. The 
 *          valid board revision range is ['A' .. 'Z'] (only the uppercase letter
 *          range is used)
 *
 *      Input   - none
 *      Output  - char cBoardRev
 */
#define PANTH_GET_BOARD_REVISION                             12



/********************************************************************************
 *  PANTH_GET_BOARD_LOCATION
 *
 *      Description
 *          Used to get the physical location of the MIB in the chassis. The
 *          first physical location will be read as 1 and the second as 0.
 *
 *      Input   - none
 *      Output  - char cBoardLocation
 */
#define PANTH_GET_BOARD_LOCATION                             13      // get board location



/********************************************************************************
 *  PANTH_GET_PICLET_OPERATING_MODE
 *
 *      Description
 *          Used to determine if the piclet is operating in appsector or
 *          bootsector mode. An output value of 0 represents bootsector mode and
 *          1 appsector.
 *
 *      Input   - char cPicletAddr
 *      Output  - char cOperatingMode
 */
#define PANTH_GET_PICLET_OPERATING_MODE                      14      // get piclet operating mode



/********************************************************************************
 *  PANTH_GET_PICLET_SOFTWARE_REV
 *
 *      Description
 *          Used to determine appsector / bootsector revision. The output of this
 *          this command varies depending on if the device is in bootsector or
 *          appsector mode.
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_GET_PICLET_SOFTWARE_REV                        15      // get boot/app rev (dependent on mode)



/********************************************************************************
 *  PANTH_SET_PICLET_485_PORT_MODE
 *
 *      Description
 *          Used to configure the PIClet for 485 operating mode
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_SET_PICLET_485_PORT_MODE                       16      // set 485/current loop mode



/********************************************************************************
 *  PANTH_SET_PICLET_CURLOOP_PORT_MODE
 *
 *      Description
 *          Used to configure the PIClet for current loop operating mode
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_SET_PICLET_CURLOOP_PORT_MODE                   17      // set 485/current loop mode



/********************************************************************************
 *  PANTH_RESET_PICLET
 *
 *      Description
 *          Used to configure the PIClet for current loop operating mode
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_RESET_PICLET                                   18      // reset piclet device



/********************************************************************************
 *  PANTH_WRITE_PICLET_FLASH
 *
 *      Description
 *          Used to configure the PIClet for current loop operating mode
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_WRITE_PICLET_FLASH                             19      // write to piclet flash



/********************************************************************************
 *  PANTH_READ_PICLET_FLASH
 *
 *      Description
 *          Used to configure the PIClet for current loop operating mode
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_READ_PICLET_FLASH                             20      // read from piclet flash



/********************************************************************************
 *  PANTH_JUMP_TO_BOOTSECT
 *
 *      Description
 *          Used to configure the PIClet for current loop operating mode
 *
 *      Input   - char cPicletAddr
 *      Output  - unsigned short usSoftwareRev
 */
#define PANTH_PICLET_JUMP_TO_BOOTSECT                        21      // jump to bootsector



/********************************************************************************
 *  PANTH_EMAIL_QUEUE_INIT
 *
 *      Description
 *          Queues the pump initialization sequence to be transmitted before the
 *          next data transmission.
 *
 *      Input   - None
 *      Output  - None
 */
#define PANTH_EMAIL_QUEUE_INIT                               22      // Email TX initialization sequence



/********************************************************************************
 *  PANTH_EMAIL_TX_DATA
 *
 *      Description
 *          Transmits data on the email port
 *
 *      Input   - unsigned char bBytesToTransmit;
 *                unsigned char bTxData[32];
 *      Output  - None
 */
#define PANTH_EMAIL_TX_DATA                                  23      // Email TX



/********************************************************************************
 *  PANTH_EMAIL_RX_DATA
 *
 *      Description
 *          Reads data from the email port
 *
 *      Input   - None
 *      Output  - unsigned char bBytesReceived;
 *                unsigned char bRxData[32];
 */
#define PANTH_EMAIL_RX_DATA                                  24      // Email RX
